#include "myfs.h"
#include <string.h>
#include <iostream>
#include <math.h>
#include <sstream>
#define FILE_SIZE 1000
#define TYPE_FILE 'f'
#define INODE_PLACE 100
#define INODE_SIZE 500
const char *MyFs::MYFS_MAGIC = "MYFS";

MyFs::MyFs(BlockDeviceSimulator *blkdevsim_):blkdevsim(blkdevsim_) 
{
	struct myfs_header header;
	blkdevsim->read(0, sizeof(header), (char *)&header);

	if (strncmp(header.magic, MYFS_MAGIC, sizeof(header.magic)) != 0 ||
	    (header.version != CURR_VERSION))
	{
		std::cout << "Did not find myfs instance on blkdev" << std::endl;
		std::cout << "Creating..." << std::endl;
		format();
		std::cout << "Finished!" << std::endl;
	}
}

void MyFs::format() 
{

	// put the header in place
	char data = 0;
	struct myfs_header header;
	strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
	header.version = CURR_VERSION;
	blkdevsim->write(0, sizeof(header), (const char*)&header);/*write(int addr, int size, const char *data)
																memcpy(filemap + addr, data, size);*/

	blkdevsim->write(INODE_PLACE,500,(const char*)&data);//saving 1000 first bytes forthe indoe table.
}

void MyFs::create_file(std::string path_str, bool directory) 
{
	struct fileInodeStruct  newFile;
	if(directory)
	{
		throw std::runtime_error("not implemented");
	}
	else
	{
		newFile.type = TYPE_FILE;
	}
	newFile.path = path_str;
	newFile.size = FILE_SIZE;


	if(_myInodeTable.empty())//first file.
	{
		newFile.start = 601;//because the headr and "inode table" takes the first 600.
	}
	else
	{
		struct fileInodeStruct fileNode = _myInodeTable.back();
		newFile.start = ( fileNode.start + FILE_SIZE + 1);
	}
	_myInodeTable.push_back(newFile);

	//writing the new table.
	blkdevsim->write(INODE_PLACE,INODE_SIZE, (const char*)&_myInodeTable);/*write(int addr, int size, const char *data)
																memcpy(filemap + addr, data, size);*/
	blkdevsim->write(newFile.start, newFile.size, (const char*) &"empty file");


//throw std::runtime_error("not implemented");
}

std::string MyFs::get_content(std::string path_str)
 {
 	std::vector <struct fileInodeStruct> getDataVector;
 	std::string contentFromFile;
 	blkdevsim->read(INODE_PLACE,INODE_SIZE,( char*)&getDataVector); /*(int addr, int size, char *ans)
						memcpy(ans, filemap + addr, size)*/

 	struct fileInodeStruct fileNode;
 	for(int i=0;i<getDataVector.size();i++)

 		fileNode = getDataVector[i];
 	{
 		if(fileNode.path == path_str)
 		{
 			blkdevsim->read(fileNode.start,FILE_SIZE,(const char*)&contentFromFile);//getting content from the file 
 			return contentFromFile;
 		}
 	}
 	return "can't find file";
	//throw std::runtime_error("not implemented");
	//return "";
}

void MyFs::set_content(std::string path_str, std::string content) 
{
	std::vector<struct fileInodeStruct> getDataVector;
 	blkdevsim->read(INODE_PLACE,INODE_SIZE,(char*)&getDataVector);
 	struct fileInodeStruct fileNode;
 	for(int i=0;i<getDataVector.size(); i++)
 	{
 		fileNode = getDataVector[i];
 		if(getDataVector[i].path == path_str)
 		{
 			blkdevsim->write(fileNode.start,FILE_SIZE,(const char*)&content);/*write(int addr, int size, const char *data)
																memcpy(filemap + addr, data, size);*/
 			return;
 
 		}
 	}
 	std::cout<<"can't find file"<<std::endl;
	//throw std::runtime_error("not implemented");
}

MyFs::dir_list MyFs::list_dir(std::string path_str) 
{
	dir_list ans;
	std::vector<struct fileInodeStruct> getDataVector;
 	blkdevsim->read(INODE_PLACE,INODE_SIZE,(char*)&getDataVector);
 	struct fileInodeStruct fileNode;
 	for(int i=0;i<getDataVector.size();i++)
 	{
 		fileNode = getDataVector[i];
 		dir_list newDirListStruct;
 		newDirListStruct.name = getDataVector[i].path;
 		newDirListStruct.is_dir= false;
 		newDirListStruct.size = getDataVector[i].size;
 		ans.push_back(newDirListStruct);
 	}
	//throw std::runtime_error("not implemented");
	return ans;
}

